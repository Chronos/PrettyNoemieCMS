<?php


class Fixtures
{
    function siteMap()
    {
        return [
            [
                'id'     => uniqid(),
                'module' => 'video',
                'fields' => $this->getModuleInitValues('video'),
            ],
			[
				'id'     => uniqid(),
				'module' => 'three_goals',
				'fields' => $this->getModuleInitValues('three_goals'),
			],
            [
                'id'     => uniqid(),
                'module' => 'button_design',
                'fields' => $this->getModuleInitValues('button_design'),
            ],
			[
				'id'     => uniqid(),
				'module' => 'button',
				'fields' => $this->getModuleInitValues('button'),
			],
            [
                'id'     => 'footer',
                'fields' => [
                    'rightText' => [
                        'type'  => 'text',
                        'value' => '<h5 class="second-color"></h5><p class="second-color">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus blanditiis explicabo facilis fuga, fugit, id impedit </p>',
                    ],
                    'leftText'  => [
                        'type'  => 'text',
                        'value' => '<h5 class="second-color">PrettyNoemieCMS</h5><p class="second-color">Made with love by <b><a href="https://www.patreon.com/robinbanquo">Robin Banquo</a></b></p></p>',
                    ],
                ],
            ],

        ];
    }

    function siteParams()
    {
        $f3 = Base::instance();

        return [
            'title'       => $f3->get('siteLabel'),
            'titleSize'   => 'medium',
            'favIcon'     => '',
            'siteLogo'    => '',
            'lang'        => $f3->get('siteLang') ? $f3->get('siteLang') : 'fr_FR',
            'description' => $f3->get('siteDescription'),
            'keywords'    => $f3->get('siteKeywords'),
            'isPublished' => 'true',
		//	'framalink' => ''
        ];
    }

    function siteOptions()
    {
        $themesList = json_decode(file_get_contents(__DIR__ . '/../config/themes.json'), true);
        $fontsList = json_decode(file_get_contents(__DIR__ . '/../config/fonts.json'), true);

        return [
            'font'   => $fontsList[0],
            'theme'  => $themesList['Classic'],
            'params' => $this->siteParams(),
        ];
    }

    function getModuleInitValues($module)
    {
        $moduleInfo = json_decode(
            file_get_contents(__DIR__ . '/../views/modules/' . $module . '/info.json'),
            true
        );

        return $moduleInfo['fields'];
    }

}
